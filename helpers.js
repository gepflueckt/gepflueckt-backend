module.exports = {
	dataRequest : (data, token = '') => {

		let headers = {
			'Content-Type' : 'application/json'
		}

		return { data: data, headers: headers }

	},

	actionMessages : query => {
		let alert = null;
		let success = query.success;
		let error = query.error;
		if (success)
			alert = { type: 'success', msg: "<b>Erfolgreich "+success+".</b>" };
		if (error)
			alert = { type: 'danger', msg: "<b>Fehler beim "+error+"!</b>" };

		return alert
	}
}
