var Config = require('./config.js'),
		express = require('express'),
		app = express(),
		cookieParser = require('cookie-parser'),
		bodyParser = require('body-parser'),
		path = require('path'),
		helpers = require('./helpers.js'),
		handlebars = require('express-handlebars'),
		hbhelpers = require('handlebars-helpers')(['comparison']),
		axios = require('axios'),
		ax = axios.create({
			baseURL: 'http://localhost:5551/api',
			headers: {'Content-Type': 'application/json'}
		}),
		port = Config.port;

require('./handlebars.js')

// Express Setup
app.use('/static', express.static('static'))
app.use(cookieParser())
app.use(bodyParser.urlencoded( { extended: true } ))
app.use(bodyParser.json())
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', handlebars({ defaultLayout: 'main' }))
app.set('view engine', 'handlebars');

// Controllers
const Auth = require('./controllers/auth');
const LoginController = require('./controllers/login');
const DashboardController = require('./controllers/dashboard');
const ProductsController = require('./controllers/products');
const TagsController = require('./controllers/tags');

app.listen(port, () => {
	console.log('Server listening on ' + port);
})

app.get('/', Auth.check(app), DashboardController.get);

app.post('/login', LoginController.post)
app.get('/login', LoginController.get)
app.get('/logout', LoginController.logout)

app.get('/dashboard', Auth.check(app), DashboardController.get)

app.get('/produkte', Auth.check(app), ProductsController.overview)
app.delete('/produkte/:id', Auth.check(app), ProductsController.delete)
app.get('/produkte/meine', Auth.check(app), ProductsController.own)
app.get('/produkte/neu', Auth.check(app), ProductsController.new)
app.post('/produkte/neu', Auth.check(app), ProductsController.newWithData)
app.post('/produkte/speichern', Auth.check(app), ProductsController.create)

app.get('/tags', Auth.check(app), TagsController.overview)
app.delete('/tags/:id', Auth.check(app), TagsController.delete)
app.get('/tags/neu', Auth.check(app), TagsController.new)
app.post('/tags/neu', Auth.check(app), TagsController.create)
