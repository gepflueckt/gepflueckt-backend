let Config = require('../config.js'),
		axios = require('axios'),
		ax = axios.create({
			baseURL: 'http://localhost:5551/api',
			headers: {'Content-Type': 'application/json'}
		}),
		helpers = require('../helpers'),
		{OperationHelper} = require('apac'),
		opHelper = new OperationHelper({
			awsId: Config.awsId,
			awsSecret: Config.awsSecret,
			assocId: Config.assocId,
			locale: 'DE'
		});

module.exports = {

	/*
	* ALL PRODUCTS
	*/
	overview : (req, res) => {

		ax.defaults.headers.common['Authorization'] = req.token;

		ax.get('/products').then(result => {
			console.log(result.data);
			res.render('productOverview', { title: 'Produkte', products: result.data })
		}).catch(err => {
			if(err.repsonse.status == 401) {
				// Cookie not valid anymore, clear and redirect to login
				res.clearCookie('gpflauthtoken');
				res.redirect('/login');
			}
			else {
				res.render('error', { back: req.headers.refferer, alert: { type: 'danger', msg: '<b>Fehler in der API.</b> Bitte sofort an mike@gepflueckt.de melden!' } })
			}
		})

	},

	/*
	* OWN PRODUCTS
	*/
	own: (req, res) => {

		let userid = req.user.id;

		ax.defaults.headers.common['Authorization'] = req.token;

		ax.get( '/products/by/' + userid )
		.then(result => {
			res.render('productOverview', { title: 'Meine Produkte', products: result.data })
		})
		.catch(err => {
			res.render('error', { back: req.headers.refferer, alert: { type: 'danger', msg: '<b>Fehler in der API (' + err.response.status + ').</b> Bitte sofort an mike@gepflueckt.de melden!' } })
		})

	},

	/*
	* NEW PRODUCT PAGE
	*/
	new: (req, res) => {
		res.render('productNew', { title: 'Neues Produkt' })
	},

	/*
	* NEW PRODUCT PAGE W/ FETCHED DATA
	*/
	newWithData: (req, res) => {

		if(req.body && req.body.amazonUrl) {

			let asin = req.body.amazonUrl.match(Config.regex.asinFromUrl);

			opHelper.execute('ItemLookup', {
				ItemId: asin,
				ResponseGroup: 'Small,Images,OfferFull'
			}).then(lkpRes => {
				// Load title
				if(lkpRes.result && lkpRes.result.ItemLookupResponse && lkpRes.result.ItemLookupResponse.Items && lkpRes.result.ItemLookupResponse.Items.Item) {
					let item = lkpRes.result.ItemLookupResponse.Items.Item;
					// Get data
					let asin = item.ASIN;
					let title = item.ItemAttributes.Title;
					let imageSet = item.ImageSets.ImageSet;

					let images = [];

					if(Array.isArray(imageSet))
						images = item.ImageSets.ImageSet.reverse();
					else
						images.push(imageSet)

					let link = item.DetailPageURL;

					let tags = []

					// Get list of tags
					ax.get( '/tags/' )
					.then(result => {

						tags = result.data;
						tags.sort((a,b) => { return (a.children.length > b.children.length) ? 1 : -1; })
						res.render('productNew', { form: true, title: title, amazonUrl: req.body.amazonUrl, images: images, link: link, asin: asin, tags: tags })

					})
					.catch(err => { throw err })
				}
				else {
					// Error message and try again..
					res.render('productNew', { title: 'Neues Produkt', amazonUrl: req.body.amazonUrl });
					return null;
				}

			}).catch(err => {
				console.error('Oh no!', err);
			})

		}
		else {
			res.render('productNew', { title: 'Neues Produkt' })
		}
	},

	/*
	* NEW PRODUCT CREATE
	*/
	create: (req, res) => {

		let data = req.body;
		data.author = req.user.id;

		ax.defaults.headers.common['Authorization'] = req.token;
		ax.post('/products', helpers.dataRequest(data))
		.then(result => {
			res.redirect('/produkte?success=erstellt');
		})
		.catch(err => { throw err; })
	},

	/*
	* DELETE PRODUCT
	*/
	delete : (req, res) => {
		ax.delete('/products/' + req.params.id)
		.then(result => { res.json({ success: true }) })
		.catch(err => { res.json({ success: false }) })
	}
}
