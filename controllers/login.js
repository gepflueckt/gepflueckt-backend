let axios = require('axios'),
		ax = axios.create({
			baseURL: 'http://localhost:5551/api',
			headers: {'Content-Type': 'application/json'}
		}),
		helpers = require('../helpers');

module.exports = {

	post : (req, res) => {

		let nick = req.body.nick;
		let password = req.body.password;

		ax.get('/login', helpers.dataRequest({ nick: nick, password: password }) ).then(result => {

			console.log(result.data);

			// Check for token
			if (result.data.success) {
				// Successful login, send cookie on client side
				res.cookie('gpflauthtoken', result.data.token);
				res.cookie('gpfluser', JSON.stringify(result.data.user));
				res.render('home', {
					content: result.data.token
				})
			}
			else {
				// Login failed
				res.render('login', { layout: 'login', alert: { type: 'danger', msg: '<b>Wrong login details.</b> Please try again' } })
			}

		}).catch(err => {
			console.log(err.request);
			res.render('login', { layout: 'login', alert: { type: 'danger', msg: '<b>Error within the API.</b> Please report immediately to tech@gepflueckt.de' } })
		})

	},

	get : (req, res) => {

		if(req.cookies.gpflauthtoken) {
			res.redirect('/dashboard');
		}
		else {
			res.render('login', { layout: 'login' })
		}
	},

	logout: (req, res) => {
		res.clearCookie('gpflauthtoken');
		res.clearCookie('gpfluser');
		res.redirect('/login');
	}
}
