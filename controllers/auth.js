module.exports = {
	check :
		(app) => {
			return (req, res, next) => {
				if(req.cookies.gpflauthtoken && req.cookies.gpfluser) {
					req.user = JSON.parse(req.cookies.gpfluser);
					req.token = req.cookies.gpflauthtoken;
					app.locals.user = req.user;
					next()
				}
				else {
					res.redirect('/login')
				}
			}
		}
}
