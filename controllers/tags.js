let Config = require('../config.js'),
		axios = require('axios'),
		ax = axios.create({
			baseURL: 'http://localhost:5551/api',
			headers: {'Content-Type': 'application/json'}
		}),
		helpers = require('../helpers');

module.exports = {

	/*
	* ALL TAGS
	*/
	overview : (req, res) => {

		let alert = helpers.actionMessages(req.query);

		ax.defaults.headers.common['Authorization'] = req.token;

		ax.get('/tags').then(result => {
			console.log(result.data);
			res.render('tagsOverview', { title: 'Tags', tags: result.data, alert: alert })
		}).catch(err => {
			if(err.repsonse.status == 401) {
				// Cookie not valid anymore, clear and redirect to login
				res.clearCookie('gpflauthtoken');
				res.redirect('/login');
			}
			else {
				res.render('error', { back: req.headers.refferer, alert: { type: 'danger', msg: '<b>Fehler in der API.</b> Bitte sofort an mike@gepflueckt.de melden!' } })
			}
		})

	},

	new : (req, res) => {

		ax.defaults.headers.common['Authorization'] = req.token;
		let tags = null;

		ax.get('/tags')
		.then(result => {
			tags = result.data;
			res.render('tagsNew', { title: 'Neuer Tag', tags: tags })
		})
		.catch(err => { throw err; })
	},

	create : (req, res) => {
		let data = req.body;
		let name = data.name;
		let parent = (data.parent) ? data.parent : null;

		if(data) {
			// Add to database here
			ax.post('/tags', helpers.dataRequest({ name: name, parent: parent }))
			.then(result => {
				res.redirect('/tags?success=erstellt');
			})
			.catch(err => { throw err; })
		}
	},

	delete : (req, res) => {
		ax.delete('/tags/' + req.params.id)
		.then(result => { res.json({ success: true }) })
		.catch(err => { res.json({ success: false }) })
	}

}
