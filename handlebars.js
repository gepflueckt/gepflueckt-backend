let Handlebars = require('handlebars');

Handlebars.registerHelper( 'readableStatus', function ( status ) {
  let readableStatus = {
		0: 'Deaktiviert',
		1: 'Entwurf',
		2: 'Wird geprüft',
		3: 'Revision',
		4: 'Abgelehnt',
		5: 'Freigegeben',
		6: 'ONLINE'
	}

	return new Handlebars.SafeString(readableStatus[status]);
});

Handlebars.registerPartial('productSmall', '{{productSmall}}')
Handlebars.registerPartial('productFull', '{{productFull}}')
